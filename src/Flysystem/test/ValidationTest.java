package flightbooking.test;

import flightbooking.gui.Validation;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class ValidationTest {
    
    public ValidationTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Test of isNameValid method, of class Validation.
     */
    @Test
    public void testIsNameValid() {
        assertTrue(Validation.isName("Olga"));
        assertTrue(Validation.isName("olga"));
        assertTrue(Validation.isName("olGa"));
        assertTrue(Validation.isName("oLgA"));
        assertTrue(Validation.isName("OLGA"));
        
        assertFalse(Validation.isName(""));
        assertFalse(Validation.isName("4562"));
        assertFalse(Validation.isName(" "));
        assertFalse(Validation.isName("."));
        assertFalse(Validation.isName("allan1234"));
        
        System.out.println("* ValidationTest: isNameValid()");
  }

    /**
     * Test of isPhoneNumber method, of class Validation.
     */
    @Test
    public void testIsPhoneNumberValid() {        
        assertTrue(Validation.isPhoneNumberValid("+1"));
        assertTrue(Validation.isPhoneNumberValid("+123456789"));
        assertTrue(Validation.isPhoneNumberValid("1"));
        assertTrue(Validation.isPhoneNumberValid("123456789"));
                
        assertFalse(Validation.isPhoneNumberValid("-1"));
        assertFalse(Validation.isPhoneNumberValid("Olga"));
        assertFalse(Validation.isPhoneNumberValid("olga"));
        assertFalse(Validation.isPhoneNumberValid("OLGA"));
        assertFalse(Validation.isPhoneNumberValid(" "));
        assertFalse(Validation.isPhoneNumberValid("."));
        assertFalse(Validation.isPhoneNumberValid("++1"));
        assertFalse(Validation.isPhoneNumberValid("1++"));
        
        System.out.println("* ValidationTest: isPhoneNumberValid()");
    }

    /**
     * Test of isEMailValid method, of class Validation.
     */
    @Test
    public void testIsEMailValid() {        
        assertTrue(Validation.isEMailValid("olgaAirlines@olga.org"));
        assertTrue(Validation.isEMailValid("olgaairlines@olga.org"));
        assertTrue(Validation.isEMailValid("olga.airlines@olga.org"));
        assertTrue(Validation.isEMailValid("olgaairlines@olga.org"));
        assertTrue(Validation.isEMailValid("OLGAAIRLINES@OLGA.ORG"));
        assertTrue(Validation.isEMailValid("olga12.3Airlines@olga123.org"));
        assertTrue(Validation.isEMailValid("olga123.Airlines@olga123.org"));
        assertTrue(Validation.isEMailValid("olga123Airlines@olga123.org"));

        assertTrue(Validation.isEMailValid("olga-airlines@olga.org"));
        assertTrue(Validation.isEMailValid("123456@789.org"));
        
        assertFalse(Validation.isEMailValid(""));
        assertFalse(Validation.isEMailValid(" "));
        assertFalse(Validation.isEMailValid("."));
        assertFalse(Validation.isEMailValid("olgaairlinesolga.org"));
        assertFalse(Validation.isEMailValid("olga airlines @ olga . org"));
        assertFalse(Validation.isEMailValid("olga"));
        assertFalse(Validation.isEMailValid("@olga.org"));
        assertFalse(Validation.isEMailValid("olgaairlines@olga.o"));
        assertFalse(Validation.isEMailValid("olga..airlines@olga.org"));
        assertFalse(Validation.isEMailValid("olgaairlines@olga.123"));
        assertFalse(Validation.isEMailValid("olgaairlines@olga..org"));
        
        System.out.println("* ValidationTest: isEMailValid()");
    }
    
    @Test
    public void testIsDateValid()
    {        
        assertTrue(Validation.isDateValid("1234-56-78"));
        
        assertFalse(Validation.isDateValid(""));
        assertFalse(Validation.isDateValid(" "));
        assertFalse(Validation.isDateValid("."));
        assertFalse(Validation.isDateValid("12345678"));
        assertFalse(Validation.isDateValid("12-34-5678"));
        assertFalse(Validation.isDateValid("-12-34"));
        assertFalse(Validation.isDateValid("1234"));
        assertFalse(Validation.isDateValid("1234-56"));
        assertFalse(Validation.isDateValid("1234-567-89"));
        assertFalse(Validation.isDateValid("1234-56-7"));
        assertFalse(Validation.isDateValid("1234-56-789"));
        assertFalse(Validation.isDateValid("123-45-67"));
        assertFalse(Validation.isDateValid("1-23-45"));
        assertFalse(Validation.isDateValid("abcd-ef-gh"));
        assertFalse(Validation.isDateValid("ABCD-EF-GH"));
        assertFalse(Validation.isDateValid("abcdefgh"));
        
        System.out.println("* ValidationTest: isDateValid()");
    }
}
