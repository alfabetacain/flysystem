package flightbooking;

public class Seat {

    private int id, row, x, y;
    private String letter;
    private SeatType seatType;
    private PlaneType planeType;
    private boolean occupied;

    public Seat(int x, int y, int row, String letter, SeatType seatType, PlaneType planeType) {
        this.x = x;
        this.y = y;
        this.row = row;
        this.letter = letter;
        this.seatType = seatType;
        this.planeType = planeType;
    }
    
    public Seat(int id, int x, int y, int row, String letter, SeatType seatType, PlaneType planeType, boolean occupied ) {
        this(x, y, row, letter, seatType, planeType);
        this.id = id;
        this.occupied = occupied;
    }

    public void setOccupied(boolean occupied) {
        this.occupied = occupied;
    }
    
    public int getId() {
        return id;
    }

    public int getRow() {
        return row;
    }

    public String getLetter() {
        return letter;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public SeatType getSeatType() {
        return seatType;
    }

    public PlaneType getPlaneType() {
        return planeType;
    }
    
    public boolean isOccupied() {
        return occupied;
    }
}
