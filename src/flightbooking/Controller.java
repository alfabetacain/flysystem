package flightbooking;

import flightbooking.gui.GUIController;
import flightbooking.gui.Validation;
import java.util.ArrayList;
import java.util.Date;

/**
 * Controller is meant for controlling the flow in the GUI
 */
public class Controller
{
    //Information for making a reservation listed by steps

    private Departure chosenDeparture;
    private ArrayList<Seat> chosenSeats;
    private ArrayList<Passenger> passengers;
    private Customer customer;
    private Reservation currentReservation;
    private SQLController sqlcontroller;
    private GUIController gui;

    private Controller()
    {
        sqlcontroller = new SQLController();
    }

    /**
     * Starts the program by creating a GUIController and
     * setting it up
     */
    public void begin()
    {
        gui = new GUIController(this);
        gui.setup();
    }

    /**
     * Returns all airports in the database by requesting
     * them from the SQLController
     * @return all airports in the database
     */
    public ArrayList<Airport> getAirports()
    {

        return sqlcontroller.getAirports();
    }

    /**
     * Returns all departures that matches the given
     * parameters
     * @param depart the date the departure will begin
     * @param arrive the date the departure will end
     * @param origin the airport from which to depart
     * @param dest the airport to arrive to
     * @return all departures matching the parameters
     */
    public ArrayList<Departure> getDepartures(Date depart, Date arrive, Airport origin, Airport dest)
    {
        ArrayList<Departure> departures = null;
        try
        {
            departures = sqlcontroller.getDepartures(depart, arrive, origin, dest);
            if (departures == null || departures.isEmpty())
            {
                throw new NullPointerException("Nothing found");
            }
        }
        catch (NullPointerException excep)
        {
            Validation.validationError(excep.getMessage());
        }
        return departures;
    }
    
    /**
     * Clears all information concerning a reservation
     * Clears the passengers, chosen seats, customer
     * and the current reservation. Furthermore it ends the 
     * current session in the GUI
     */
    public void clearAll()
    {
        passengers = null;
        chosenSeats = null;
        customer = null;
        currentReservation = null;
        gui.endSession();
    }

    /**
     * Updates the seat which is located at the given coordinates
     * If it is not taken, it will become occupied. If it is a chosen seat
     * it will become unoccupied. If it is already taken by someone else
     * nothing will happen
     * @param x the x coordinate of the click
     * @param y the y coordinate of the click
     */
    public void seatUpdate(int x, int y)
    {
        try
        {
            if (chosenSeats == null)
            {
                chosenSeats = new ArrayList<>();
            }
            if (chosenDeparture == null)
            {
                throw new NullPointerException("departure is null");
            }
            boolean notFound = true;
            //Searches for a seat which is where the user clicked
            ArrayList<Seat> seats = chosenDeparture.getSeats();
            for (int i = 0; i < seats.size() && notFound; ++i)
            {
                Seat currentSeat = seats.get(i);
                //Checks whether the click is within the x-coordinates of the seat
                if (x <= currentSeat.getX()
                        + currentSeat.getSeatType().getWidth()
                        && x >= currentSeat.getX())
                {
                    //Check whether the click is within the y-coordinates of the seat
                    if (y <= currentSeat.getY()
                            + currentSeat.getSeatType().getHeight()
                            && y >= currentSeat.getY())
                    {
                        notFound = false;
                        if (!(currentSeat.isOccupied()))
                        {
                            if (chosenSeats.contains(currentSeat))
                            {
                                chosenSeats.remove(currentSeat);
                            }
                            else
                            {
                                chosenSeats.add(currentSeat);
                            }
                        }
                        gui.updateStep();
                    }
                }
            }
        }
        catch (NullPointerException excep)
        {
            System.out.println(excep.toString());
            excep.printStackTrace();
        }
    }

    /**
     * Sets the departure field with the given parameter
     * Exception is thrown if departure is null after the set
     * Furthermore continues the current session in the GUI
     * @param departure 
     */
    public void setDeparture(Departure departure)
    {
        try
        {
            chosenDeparture = departure;
            if (chosenDeparture == null)
            {
                throw new NullPointerException("Departure is null");
            }
            chosenSeats = new ArrayList<>();
        }
        catch (NullPointerException excep)
        {
            System.out.println(excep.toString());
            excep.printStackTrace();
        }
        gui.next();
    }

    /**
     * Sets the passengers field by combining the first and last
     * names lists given through parameters to one list of passengers
     * A passenger object is created through the combining
     * @param firstNames the first names of the passengers
     * @param lastNames the last names of the passengers
     */
    public void setPassengers(String[] firstNames, String[] lastNames)
    {
        try
        {
            if (chosenSeats == null || chosenSeats.isEmpty())
            {
                throw new NullPointerException("Seats is null");
            }
            if (firstNames == null || firstNames.length == 0)
            {
                throw new NullPointerException("firstNames array is null or empty");
            }
            if (lastNames == null || lastNames.length == 0)
            {
                throw new NullPointerException("lastNames array is null or empty");
            }
            if (firstNames.length != lastNames.length)
            {
                throw new IllegalStateException("Name arrays not of equal size");
            }
            passengers = new ArrayList<>();
            for (int i = 0; i < firstNames.length; ++i)
            {
                passengers.add(new Passenger(firstNames[i],
                        lastNames[i], chosenSeats.get(i)));
            }
            gui.next();
        }
        catch (IllegalStateException | NullPointerException excep)
        {
            System.out.println(excep.toString());
            excep.printStackTrace();
        }
    }

    /**
     * Sets the customer field by creating a Customer from the given
     * parameters and then setting the field
     * @param firstName the first name of the customer
     * @param lastName the last name of the customer
     * @param adress the address of the customer
     * @param phone the phone number of the customer
     * @param mail the mail of the customer
     */
    public void setCustomer(String firstName, String lastName, String adress,
            String phone, String mail)
    {
        Customer cust = sqlcontroller.getCustomer(phone);
        if (cust == null)
        {
            customer = new Customer(firstName, lastName, adress, phone, mail);
        }
        else
        {
        customer = cust;
        }
    }

    /**
     * Creates a reservation from the information contained in the
     * relevant fields. Then saves it to the database if a reservation
     * object is created
     */
    public void finalizeReservation()
    {
        try
        {
            Reservation temp = new Reservation(passengers, customer, chosenDeparture);
            if (temp == null)
            {
                throw new NullPointerException("Reservation not created");
            }
            sqlcontroller.saveReservation(temp);
            clearAll();
        }
        catch (NullPointerException excep)
        {
            System.out.println(excep.getMessage());
            excep.printStackTrace();
        }
    }

    /**
     * Returns the customer field
     * @return the content of the customer field
     */
    public Customer getCustomer()
    {
        return customer;
    }

    /**
     * Returns the passenger list
     * @return the content of the passengers field
     */
    public ArrayList<Passenger> getPassengers()
    {
        return passengers;
    }

    /**
     * Returns the chosen departure
     * @return the content of the departure field
     */
    public Departure getDeparture()
    {
        return chosenDeparture;
    }

    /**
     * Returns the chosen seats
     * @return returns the content of the chosen seats field
     */
    public ArrayList<Seat> getChosenSeats()
    {
        return chosenSeats;
    }

    /**
     * Returns the GUIController
     * @return the content of the GUIController field
     */
    public GUIController getGUIController()
    {
        return gui;
    }

    /**
     * Returns a list of reservations which customer matches
     * the phone number given. From the reservations the departure can
     * be extracted
     * @param phone the phone number of the customer
     * @return a list of reservations that matches the customer
     * @throws NullPointerException when nothing is found in the database
     */
    public ArrayList<Reservation> getDeparturesByPhone(String phone) throws NullPointerException
    {
            Customer cust = sqlcontroller.getCustomer(phone);
            if (cust == null)
            {
                throw new NullPointerException("No reservations.");
            }
            return sqlcontroller.getReservations(sqlcontroller.getCustomer(phone));
    }

    /**
     * Sets the reservation field with the given parameter
     * Furthermore readies the GUIController for deletion
     * @param res the reservation to be set
     */
    public void setReservation(Reservation res)
    {
        if (res == null)
        {
            System.out.println("Reservationn null in controller");
        }
        currentReservation = res;
        gui.makeReadyForDelete();
    }

    /**
     * Returns the current reservation
     * @return the content of the reservation field
     */
    public Reservation getReservation()
    {
        return currentReservation;
    }

    /**
     * Deletes the current reservation if it is not null
     * Then clears all
     */
    public void deleteReservation()
    {
        if (currentReservation == null) System.out.println("current reservation is null");
        sqlcontroller.deleteReservation(currentReservation);
        clearAll();
    }

    /**
     * The main method which starts the program by creating a controller
     * and calling the setup method
     * @param args nothing useful here
     */
    public static void main(String[] args)
    {
        Controller program = new Controller();
        program.begin();
    }
}