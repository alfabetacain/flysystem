package flightbooking;

import java.util.ArrayList;


public class Reservation 
{
    private int id;
    private ArrayList<Passenger> passengers;
    private Customer customer;
    private Departure departure;

    public Reservation(ArrayList<Passenger> passengers, Customer customer, Departure departure) {
        this.passengers = passengers;
        this.customer = customer;
        this.departure = departure;
    }
    
    public Reservation(int id, ArrayList<Passenger> passengers, Customer customer, Departure departure) {
        this(passengers, customer, departure);
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public ArrayList<Passenger> getPassengers()
    {
        return passengers;
    }

    public Customer getCustomer()
    {
        return customer;
    }

    public Departure getDeparture()
    {
        return departure;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    public void setPassengers(ArrayList<Passenger> passengers) {
        this.passengers = passengers;
    }
    
    public void setCustomer(Customer customer)
    {
        this.customer = customer;
    }    
}