CREATE TABLE airport (
    id int NOT NULL AUTO_INCREMENT,
    name varchar(64) NOT NULL,
    country varchar(64) NOT NULL,
    city varchar(64) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE departure (
    id int NOT NULL AUTO_INCREMENT,
    departure datetime NOT NULL,
    arrival datetime NOT NULL,
    originId int NOT NULL,
    destinationId int NOT NULL,
    planeId int NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE plane (
    id int NOT NULL AUTO_INCREMENT,
    name varchar(64) NOT NULL,
    planeTypeId int NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE planeType (
    id int NOT NULL AUTO_INCREMENT,
    name enum('BIG', 'SMALL') NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE seat (
    id int NOT NULL AUTO_INCREMENT,
    x int NOT NULL,
    y int NOT NULL,
    row int NOT NULL,
    letter varchar(1) NOT NULL,
    seatTypeId int NOT NULL,
    planeTypeId int NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE seatType (
    id int NOT NULL AUTO_INCREMENT,
    name enum('FIRSTCLASS', 'BUSINESS', 'ECONOMY') NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE passenger (
    id int NOT NULL AUTO_INCREMENT,
    firstName varchar(64) NOT NULL,
    lastName varchar(64) NOT NULL,
    seatId int NOT NULL,
    reservationId int NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE reservation (
    id int NOT NULL AUTO_INCREMENT,
    departureId int NOT NULL,
    customerId int NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE customer (
    id int NOT NULL AUTO_INCREMENT,
    firstName varchar(64) NOT NULL,
    lastName varchar(64) NOT NULL,
    adress varchar(64) NOT NULL,
    phone varchar(64) NOT NULL,
    mail varchar(64),
    PRIMARY KEY (id)
);