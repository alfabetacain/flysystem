SELECT
    s.id AS seatId, s.row, s.letter, s.x, s.y,
    st.name AS seatType,
    pt.name AS planeType
FROM seat s
INNER JOIN seatType st
    ON s.seatTypeId = st.id
INNER JOIN planeType pt
    ON s.planeTypeId = pt.id
WHERE pt.name = '%s'