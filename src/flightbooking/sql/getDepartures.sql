SELECT
    dep.id, dep.departure, dep.arrival,
    ori.id AS originId, ori.name AS originName, ori.country AS originCountry, ori.city AS originCity,
    des.id AS destinationId, des.name AS destinationName, des.country AS destinationCountry, des.city AS destinationCity,
    plt.name AS planeType
FROM departure dep
INNER JOIN airport ori
    ON dep.originId = ori.id
INNER JOIN airport des
    ON dep.destinationId = des.id
INNER JOIN plane pla
    ON dep.planeId = pla.id
INNER JOIN planeType plt
    ON pla.planeTypeId = plt.id
WHERE
    ori.name LIKE '%s' AND
    des.name LIKE '%s' AND
    dep.departure BETWEEN '%s' AND '%s';