SELECT
    res.id,
    dep.id, dep.departure, dep.arrival,
    ori.id AS originId, ori.name AS originName, ori.country AS originCountry, ori.city AS originCity,
    des.id AS destinationId, des.name AS destinationName, des.country AS destinationCountry, des.city AS destinationCity,
    plt.name AS planeType
FROM reservation res
INNER JOIN departure dep
    ON res.departureId = dep.id
INNER JOIN airport ori
    ON dep.originId = ori.id
INNER JOIN airport des
    ON dep.destinationId = des.id
INNER JOIN plane pla
    ON dep.planeId = pla.id
INNER JOIN planeType plt
    ON pla.planeTypeId = plt.id
WHERE res.customerId = %d;