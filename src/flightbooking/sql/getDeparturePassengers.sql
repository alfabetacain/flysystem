SELECT
    p.id AS passengerId, p.firstName, p.lastName, p.seatId,
    s.id AS seatId, s.row, s.letter, s.x, s.y,
    st.name AS seatType,
    pt.name AS planeType
FROM departure d
INNER JOIN reservation r
    ON d.id = r.departureId
INNER JOIN passenger p
    ON r.id = p.reservationId
INNER JOIN seat s
    ON p.seatId = s.id
INNER JOIN seatType st
    ON s.seatTypeId = st.id
INNER JOIN planeType pt
    ON s.planeTypeId = pt.id
WHERE d.id = %d;