INSERT INTO airport (name, country, city) VALUES
    ('Chek Lap Kok Airport', 'China', 'Hong Kong'),
    ('Copenhagen Airport', 'Denmark', 'Cophenhagen'),
    ('Fiumicino Leonardo Da Vinci Airport', 'Italy', 'Rome'),
    ('Venice Marco Polo Airport', 'Italy', 'Venice'),
    ('Berlin Tegel Airport', 'Germany', 'Berlin');

INSERT INTO departure (departure, arrival, originId, destinationId, planeId) VALUES
    ('2014-01-01 00:00:00', '2014-01-01 02:30:00', 1, 2, 1),
    ('2014-02-01 00:00:00', '2014-02-01 02:30:00', 2, 3, 2);

INSERT INTO plane (name, planeTypeId) VALUES
    ('SmallPlane051213', 1),
    ('BigPlane28113', 2);

INSERT INTO planeType (name) VALUES
    ('SMALL'),
    ('BIG');

INSERT INTO seatType (name) VALUES
    ('ECONOMY'),
    ('BUSINESS'),
    ('FIRSTCLASS');

INSERT INTO passenger (firstName, lastName, seatId, reservationId) VALUES
    ('Jens', 'Jensen', 1, 1),
    ('Anders', 'Andersen', 2, 1),
    ('Eskild', 'Eskildsen', 3, 2),
    ('Louis', 'Armstrong', 1, 2);

INSERT INTO reservation (departureId, customerId) VALUES
    (1, 1),
    (2, 1);

INSERT INTO customer (firstName, lastName, adress, phone, mail) VALUES
    ('Christian', 'Christiansen', 'Christiansvej 99 9999 Christiansstad', '+4599999999', 'christan@christiansen.dk');