package flightbooking;

public enum PlaneType
{
    SMALL(1), BIG(2);
    
    private int id;
    
    private PlaneType(int id) {
        this.id = id;
    }
    
    public int getId() {
        return id;
    }
}
