package flightbooking;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

public class SQLMapper {

    public ArrayList<Departure> getDepartures(Statement statement) {
        ArrayList<Departure> departures = new ArrayList<>();
        try (ResultSet rs = statement.getResultSet()) {
            while (rs.next()) {
                Departure departure = getDeparture(rs);
                departures.add(departure);
            }
        } catch (SQLException ex) {
            System.out.println("SQL exception: " + ex);
        }
        return departures;
    }

    public ArrayList<Seat> getSeats(Statement statement) {
        ArrayList<Seat> seats = new ArrayList<>();
        try (ResultSet rs = statement.getResultSet()) {
            while (rs.next()) {
                seats.add(getSeat(rs));
            }
        } catch (SQLException ex) {
            System.out.println("SQL exception: " + ex);
        }
        return seats;
    }

    public ArrayList<Passenger> getPassengers(Statement statement) {
        ArrayList<Passenger> passengers = new ArrayList<>();
        try (ResultSet rs = statement.getResultSet()) {
            while (rs.next()) {
                // Seat
                Seat seat = getSeat(rs);
                // Passenger
                int passengerId = rs.getInt("passengerId");
                String firstName = rs.getString("firstName");
                String lastName = rs.getString("lastName");
                passengers.add(new Passenger(passengerId, firstName, lastName, seat));
                
            }
        } catch (SQLException ex) {
            System.out.println("SQL exception: " + ex);
        }
        return passengers;
    }
    
    private Seat getSeat(ResultSet rs) {
        Seat seat = null;
        try {
            int seatId = rs.getInt("seatId");
            int x = rs.getInt("x");
            int y = rs.getInt("y");
            int row = rs.getInt("row");
            String letter = rs.getString("letter");
            SeatType seatType = SeatType.valueOf(rs.getString("seatType"));
            PlaneType planeType = PlaneType.valueOf(rs.getString("planeType"));
            seat = new Seat(seatId, x, y, row, letter, seatType, planeType, false);
        } catch (SQLException ex) {
            System.out.println("SQL exception: " + ex);
        }
        return seat;
    }
    
    public ArrayList<Airport> getAirports(Statement statement) {
        ArrayList<Airport> airports = new ArrayList<>();
        try (ResultSet rs = statement.getResultSet()) {
            while (rs.next()) {
                int id = rs.getInt("id");
                String name = rs.getString("name");
                String country = rs.getString("country");
                String city = rs.getString("city");
                airports.add(new Airport(id, name, country, city));
            }
        } catch (SQLException ex) {
            System.out.println("SQL exception: " + ex);
        }
        return airports;
    }
    
    public Customer getCustomer(Statement statement) {
        Customer customer = null;
        try (ResultSet rs = statement.getResultSet()) {
            while (rs.next()) {
                int id = rs.getInt("id");
                String firstName = rs.getString("firstName");
                String lastName = rs.getString("lastName");
                String adress = rs.getString("adress");
                String phone = rs.getString("phone");
                String mail = rs.getString("mail");
                customer = new Customer(id, firstName, lastName, adress, phone, mail);
            }
        } catch (SQLException ex) {
            System.out.println("SQL exception: " + ex);
        }
        return customer;
    }
    
    public ArrayList<Reservation> getReservations(Statement statement) {
        ArrayList<Reservation> reservations = new ArrayList<>();
        try (ResultSet rs = statement.getResultSet()) {
            while (rs.next()) {
                // Create departure
                Departure departure = getDeparture(rs);
                // Create reservation
                int id = rs.getInt("id");
                reservations.add(new Reservation(id, null, null, departure));
            }
        } catch (SQLException ex) {
            System.out.println("SQL exception: " + ex);
        }
        return reservations;
    }
    
    private Departure getDeparture(ResultSet rs) {
        Departure departure = null;
        try {
            int id = rs.getInt("id");
            Date dep = rs.getTimestamp("departure");
            Date arrival = rs.getTimestamp("arrival");
            Airport origin = new Airport(
                    rs.getInt("originId"),
                    rs.getString("originName"),
                    rs.getString("originCountry"),
                    rs.getString("originCity"));
            Airport destination = new Airport(
                    rs.getInt("destinationId"),
                    rs.getString("destinationName"),
                    rs.getString("destinationCountry"),
                    rs.getString("destinationCity"));
            PlaneType planeType = PlaneType.valueOf(rs.getString("planeType"));
            Plane plane = new Plane(planeType);
            departure = new Departure(id, plane, dep, arrival, origin, destination);
        } catch (SQLException ex) {
            System.out.println("SQL exception: " + ex);
        }
        return departure;
    }
}