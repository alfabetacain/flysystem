package flightbooking;

import java.util.ArrayList;

public class SeatCreator
{

    /**
     * Creates seats for insertion in the database.
     * 
     * @return a list of seats
     */
    public static ArrayList<Seat> getSeats()
    {
        ArrayList<Seat> seats = new ArrayList<>();
        int seatRow = 1;
        int seatRowTemp = 0 + seatRow;
        PlaneType planeType = PlaneType.BIG;
        SeatType seatType = SeatType.FIRSTCLASS;
        seatRowTemp = 0 + seatRow;
        //Draw 1st class
        String value = "A";
        int charValue = value.charAt(0);
        String next = String.valueOf((char) charValue);
        for (int y = 110; y < 295; y += 30)
        {
            if (y == 170)
            {
                y += 65;
            }
            seatRow = seatRowTemp;
            for (int x = 220; x < 300; x += 45)
            {

                //g.drawRect(x, y, 25, 25);

                seats.add(new Seat(x, y, seatRow, next, seatType, planeType));

                ++seatRow;
            }
            next = String.valueOf((char) (++charValue));
        }

        seatType = SeatType.BUSINESS;
        seatRowTemp = 0 + seatRow;
        //Draw business class
        value = "A";
        charValue = value.charAt(0);
        next = String.valueOf((char) charValue);
        for (int y = 100; y < 300; y += 22)
        {
            if (y == 166)
            {
                y += 68;
            }
            seatRow = seatRowTemp;
            for (int x = 315; x < 500; x += 37)
            {
                //g.drawRect(x, y, 22, 22);
                seats.add(new Seat(x, y, seatRow, next, seatType, planeType));

                ++seatRow;
            }
            next = String.valueOf((char) (++charValue));
        }
        seatType = SeatType.ECONOMY;
        value = "A";
        charValue = value.charAt(0);
        next = String.valueOf((char) charValue);
        for (int y = 100; y < 300; y += 18)
        {
            if (y == 172)
            {
                y += 56;
            }
            seatRow = seatRowTemp;
            for (int x = 512; x < 800; x += 30)
            {
                //g.drawRect(x, y, 18, 18);
                //System.out.println("X: " + x + "\nY: " + y + "\nSize: 18");
                seats.add(new Seat(x, y, seatRow, next, seatType, planeType));

                ++seatRow;
            }
            next = String.valueOf((char) (++charValue));
        }

        planeType = PlaneType.SMALL;
        seatRow = 1;
        seatRowTemp = 0 + seatRow;
        //Draw economy class, 18 times 6 seats
        seatType = SeatType.FIRSTCLASS;
        //Draw first class, 5 times 2 seats
        value = "A";
        charValue = value.charAt(0);
        next = String.valueOf((char) charValue);
        for (int y = 125;
                y < 275; y += 30)
        {
            if (y == 185)
            {
                y += 35;
            }
            seatRow = seatRowTemp;
            for (int x = 200; x < 320; x += 30)
            {
                seats.add(new Seat(x, y, seatRow, next, seatType, planeType));
                ++seatRow;
            }
            next = String.valueOf((char) (++charValue));
        }
        seatType = SeatType.ECONOMY;
        seatRowTemp = 0 + seatRow;
        value = "A";
        charValue = value.charAt(0);
        next = String.valueOf((char) charValue);

        for (int y = 125;
                y < 275; y += 19)
        {
            if (y == 182)
            {
                y += 37;
            }
            seatRow = seatRowTemp;
            for (int x = 350; x < 700; x += 30)
            {

                seats.add(new Seat(x, y, seatRow, next, seatType, planeType));

                ++seatRow;
            }
            next = String.valueOf((char) (++charValue));
        }
        
        return seats;
    }
}
