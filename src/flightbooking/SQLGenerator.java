package flightbooking;

import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.Scanner;

public class SQLGenerator {
    
    /**
     * Get statement for retrieving a list of departures that fit within the
     * given criteria.
     *
     * @param startDate the beginning of date interval (inclusive)
     * @param stopDate the end of date interval (inclusive)
     * @param origin the airport of departure
     * @param destination the airport of arrival
     * @return statement
     */
    public String getDepartures(Date departure, Date arrival, Airport origin, Airport destination) {
        String format = getSQLSingle("getDepartures");
        SimpleDateFormat simple = new SimpleDateFormat("yyyy-MM-dd");
        return String.format(
                format,
                origin.getName(),
                destination.getName(),
                simple.format(departure),
                simple.format(arrival));
    }

    /**
     * Get statement for getting all seats on a plane with the given planeType.
     *
     * @param planeType type of the plane
     * @return statement
     */
    public String getSeats(PlaneType planeType) {
        String format = getSQLSingle("getSeats");
        return String.format(format, planeType.toString());
    }

    /**
     * Get statement for getting all Passengers associated with a Departure.
     *
     * @param departure a Departure
     * @return statement
     */
    public String getPassengers(Departure departure) {
        String format = getSQLSingle("getDeparturePassengers");
        return String.format(format, departure.getId());
    }
    
    /**
     * Get statement for getting all Passengers associated with a Reservation.
     *
     * @param reservation a Reservation
     * @return statement
     */
    public String getPassengers(Reservation reservation) {
        String format = getSQLSingle("getReservationPassengers");
        return String.format(format, reservation.getId());
    }
    
    /**
     * 
     * @param phone
     * @return 
     */
    public String getCustomer(String phone) {
        String format = getSQLSingle("getCustomer");
        return String.format(format, phone);
    }
    
    public String getReservations(int customerId) {
        String format = getSQLSingle("getReservations");
        return String.format(format, customerId);
    }

    /**
     * Get statement for saving a customer.
     *
     * @param customer the customer
     * @return the statement
     */
    public String saveCustomer(Customer customer) {
        String format = getSQLSingle("saveCustomer");
        return String.format(
                format,
                customer.getFirstName(),
                customer.getLastName(),
                customer.getAdress(),
                customer.getPhone(),
                customer.getMail());
    }

    /**
     * Get statement for saving a reservation.
     *
     * @param departureId the id of the associated departure
     * @param customerId the id of the associated customer
     * @return the statement
     */
    public String saveReservation(int departureId, int customerId) {
        String format = getSQLSingle("saveReservation");
        return String.format(
                format,
                departureId,
                customerId);
    }

    /**
     * Get statement for saving a passenger.
     *
     * @param passenger the passenger
     * @param reservationId the id of the associated reservation
     * @return the statement
     */
    public String savePassengers(ArrayList<Passenger> passengers, int reservationId) {
        String sql = getSQLSingle("savePassengers");
        String format = "('%s', '%s', %d, %d)";
        Iterator<Passenger> iterator = passengers.iterator();
        while(iterator.hasNext()) {
            Passenger passenger = iterator.next();
            sql += "\n";
            sql += String.format(
                format,
                passenger.getFirstName(),
                passenger.getLastName(),
                passenger.getSeat().getId(),
                reservationId);
            if (iterator.hasNext()) {
                sql += ",";
            } else {
                sql += ";";
            }
        }
        return sql;
    }

    /**
     * Get statement for saving a seat.
     *
     * @param seat
     * @return
     */
    public String saveSeats(ArrayList<Seat> seats) {
        String sql = getSQLSingle("saveSeats");
        String format = "(%d, %d, %d, '%s', %d, %d)";
        Iterator<Seat> iterator = seats.iterator();
        while (iterator.hasNext()) {
            Seat seat = iterator.next();
            sql += "\n";
            sql += String.format(
                    format,
                    seat.getX(),
                    seat.getY(),
                    seat.getRow(),
                    seat.getLetter(),
                    seat.getSeatType().getId(),
                    seat.getPlaneType().getId());
            if (iterator.hasNext()) {
                sql += ",";
            } else {
                sql += ";";
            }
        }
        return sql;
    }
    
    public String deleteReservation(Reservation reservation) {
        String format = getSQLSingle("deleteReservation");
        return String.format(format, reservation.getId());
    }
    
    public String deletePassenger(Passenger passenger) {
        String format = getSQLSingle("deletePassenger");
        return String.format(format, passenger.getId());
    }
    
    public String updatePassenger(Passenger passenger) {
        String format = getSQLSingle("updatePassenger");
        return String.format(
                format,
                passenger.getFirstName(),
                passenger.getLastName(),
                passenger.getSeat().getId(),
                passenger.getId());
    }

    /**
     * Get the SQL file with the given name.
     *
     * @param filename name of file
     * @return file formatted as a list of strings using ";" as delimiter
     */
    public ArrayList<String> getSQLList(String filename) {
        ArrayList<String> list = new ArrayList<>();
        try (Scanner scanner = getScanner(filename).useDelimiter(";")) {
            while (scanner.hasNext()) {
                list.add(scanner.next() + ";");
            }
        }
        return list;
    }

    /**
     * Get the SQL file with the given name.
     *
     * @param filename name of file
     * @return file formatted as a single string
     */
    public String getSQLSingle(String filename) {
        try (Scanner scanner = getScanner(filename).useDelimiter("\\Z")) {
            return scanner.next();
        }
    }
    
    private Scanner getScanner(String filename) {
        String path = "/flightbooking/sql/" + filename + ".sql";
        InputStream inputStream = getClass().getResourceAsStream(path);
        return new Scanner(inputStream);
    }
}