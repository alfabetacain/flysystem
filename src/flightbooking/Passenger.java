package flightbooking;

public class Passenger extends Person {

    int id;
    private Seat seat;

    public Passenger(String firstName, String lastName, Seat seat) {
        super(firstName, lastName);
        this.seat = seat;
    }

    public Passenger(int id, String firstName, String lastName, Seat seat) {
        this(firstName, lastName, seat);
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public Seat getSeat() {
        return seat;
    }
}