package flightbooking;

/**
 * The customer class which extends the person class
 * Can be mapped to the customer entity in the database
 */
public class Customer extends Person {

    int id;
    String adress, phone, mail;

    /**
     * Constructor. Creates a customer from the parameters and calls its
     * super in the process
     * @param firstName the first name of the customer
     * @param lastName the last name of the customer
     * @param adress the address of the customer
     * @param phone the phone number of the customer
     * @param mail the mail of the customer
     */
    public Customer(String firstName, String lastName, String adress, String phone, String mail) {
        super(firstName, lastName);
        this.adress = adress;
        this.phone = phone;
        this.mail = mail;
    }
    
    /**
     * A second constructor, adds an id
     * @param id the id of the customer
     * @param firstName the first name of the customer
     * @param lastName the last name of the customer
     * @param adress the address of the customer
     * @param phone the phone number of the customer
     * @param mail the mail of the customer
     */
    public Customer(int id, String firstName, String lastName, String adress, String phone, String mail) {
        this(firstName, lastName, adress, phone, mail);
        this.id = id;
    }

    /**
     * Returns the content of the id field
     * @return the id field
     */
    public int getId() {
        return id;
    }
    
    /**
     * Returns the content of the address field
     * @return the address field
     */
    public String getAdress() {
        return adress;
    }

    /**
     * Returns the content of the phone field
     * @return the phone field
     */
    public String getPhone() {
        return phone;
    }

    /**
     * Returns the content of the mail field
     * @return the mail field
     */
    public String getMail() {
        return mail;
    }
}