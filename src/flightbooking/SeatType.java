package flightbooking;

public enum SeatType {

    ECONOMY(1, 18, 18), BUSINESS(2, 22, 22), FIRSTCLASS(3, 25, 25);
    
    private int id;
    private int width;
    private int height;

    private SeatType(int id, int width, int height) {
        this.id = id;
        this.width = width;
        this.height = height;
    }

    public int getId() {
        return id;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }
}
