package flightbooking;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class SQLExecutor {

    private static SQLExecutor executor;
    private Connection connection;

    /**
     * Creates a new SQLExectutor.
     */
    private SQLExecutor() {
    }

    /**
     * Follows singleton pattern.
     *
     * @return an SQLExecutor
     */
    public static SQLExecutor get() {
        if (executor == null) {
            executor = new SQLExecutor();
        }
        return executor;
    }
    
    private Connection getConnection() {
        String url = "jdbc:mysql://mysql.itu.dk:3306/flight_714_sydney";
        String usr = "olga";
        String pwd = "snullerbasse";
        
        try {
            if (connection == null) {
                connection = DriverManager.getConnection(url, usr, pwd);
            } else if (!connection.isValid(1)) {
                connection.close();
                connection = DriverManager.getConnection(url, usr, pwd);
            }
        } catch (SQLException ex) {
            System.out.println("SQL exception: " + ex);
        }
        
        return connection;
    }

    /**
     * Executes a query (SELECT statement).
     *
     * @param sql statement to be executed
     *
     * @return a set of results
     */
    public Statement executeQuery(String sql) {
        Statement statement = null;
        try {
            statement = getConnection().createStatement();
            statement.executeQuery(sql);
            statement.closeOnCompletion();
        } catch (SQLException ex) {
            System.out.println("SQL exception: " + ex);
        }
        return statement;
    }
    
    /**
     * Executes several statements.
     *
     * @param sqls a list of statements to be executed
     */
    public void executeBatch(ArrayList<String> sqls) {
        try (Statement statement = getConnection().createStatement()) {
            for (String sql : sqls) {
                statement.addBatch(sql);
            }
            statement.executeBatch();
        } catch (SQLException ex) {
            System.out.println("SQL exception: " + ex);
        }
    }
    
    /**
     * Executes an update (INSERT, UPDATE or DELETE statement).
     * 
     * @param sql
     * @return 
     */
    public int executeUpdate(String sql) {
        int key = 0;
        try (Statement statement = getConnection().createStatement()) {
            statement.executeUpdate(sql, Statement.RETURN_GENERATED_KEYS);
            ResultSet rs = statement.getGeneratedKeys();
            if (rs.next()) {
                key = rs.getInt(1);
            }
        } catch (SQLException ex) {
            System.out.println("SQL exception: " + ex);
        }
        return key;
    }
}