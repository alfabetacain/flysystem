package flightbooking.gui;

import flightbooking.Controller;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JPanel;

public class NavigationPanel extends JPanel {

    private JButton previous;
    private JButton next;
    
    public NavigationPanel() {
        BoxLayout layout = new BoxLayout(this, BoxLayout.X_AXIS);
        setLayout(layout);
    }
    
    public void setup(final Controller controller)
    {
        final GUIController temp = controller.getGUIController();
        JButton cancelButton = new JButton("Cancel");
        cancelButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                controller.clearAll();
            }
        });
        add(cancelButton);

        add(Box.createHorizontalGlue());

        previous = new JButton("Previous");
        previous.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                temp.previous();
            }
        });
        add(previous);

        next = new JButton("Next");
        next.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                temp.getCurrentPanel().validate();
            }
        });
        add(next);
    }
    
    public void enablePrevious()
    {
        previous.setEnabled(true);
    }
    
    public void disablePrevious()
    {
        previous.setEnabled(false);
    }
    
    public void changeToDelete()
    {
        next.setText("Delete");
    }
    
    public void changeToNext()
    {
        next.setText("Next");
    }
}
