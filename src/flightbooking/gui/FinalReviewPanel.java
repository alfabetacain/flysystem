package flightbooking.gui;

import flightbooking.Controller;
import flightbooking.Customer;
import javax.swing.JPanel;

public class FinalReviewPanel extends InfoPanel implements GUIPanel {

    public FinalReviewPanel(Controller controller) {
        super(controller);
        this.controller = controller;
        
        addLabelField("First name", "firstName", false);
        addLabelField("Last name", "lastName", false);
        addLabelField("Adress", "adress", false);
        addLabelField("Phone number", "phone", false);
        addLabelField("Email adress", "email", false);
    }

    @Override
    public JPanel getPanel() {
        return this;
    }

    @Override
    public void setup() {
        super.setup();
        
        Customer customer = controller.getCustomer();
        
        fields.get("firstName").setText(customer.getFirstName());
        fields.get("lastName").setText(customer.getLastName());
        fields.get("adress").setText(customer.getAdress());
        fields.get("phone").setText(customer.getPhone());
        fields.get("email").setText(customer.getMail());
    }

    @Override
    public void update() {
        setup();
    }
    
    @Override
    public void validate()
    {
        controller.finalizeReservation();
    }
}
