package flightbooking.gui;

import flightbooking.Controller;
import flightbooking.Seat;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JPanel;

/**
 * Class for drawing a plane. It can draw either planes of types BIG or small.
 * The type is retrieved from the controller. It extends JPanel, so it
 * can be inserted into a JFrame
 */
public class PlaneDrawingPanel extends JPanel
{
    private Controller controller;

    /**
     * Constructs the plane drawing.
     * @param controller the controller from which data for the drawing is
     * collected
     */
    public PlaneDrawingPanel(final Controller controller)
    {
        //Adds the controller
        this.controller = controller;
        //Adds a mouselistener, so seat selection can be registred
        addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e)
            {
                controller.seatUpdate(e.getX(), e.getY());
            }
        });
    }

    /**
     * Returns the preferred size of the panel
     * @return a Dimension object containing the preferred size
     */
    @Override
    public Dimension getPreferredSize()
    {
        return new Dimension(900, 400);
    }

    /**
     * paints the chosen plane type and then the seats
     * @param g the Graphics object on which to draw
     */
    @Override
    public void paintComponent(Graphics g)
    {
        super.paintComponent(g);
        paintLegend(g);
        switch (controller.getDeparture().getPlane().getType())
        {
            case BIG:
                paintBigPlane(g);
                break;
            case SMALL:
                paintSmallPlane(g);
                break;
        }
        drawSeats(g);
    }
    
    /**
     * Paints a big plane
     * @param g the graphics on which to draw 
     */
    public void paintBigPlane(Graphics g)
    {
        g.drawLine(200, 100, 800, 100);
        g.drawLine(200, 300, 800, 300);
        g.drawLine(800, 100, 800, 300);
        g.drawLine(500, 100, 500, 300);
        g.drawLine(200, 100, 200, 300);
        g.drawLine(300, 100, 300, 300);
        g.drawArc(0, 100, 400, 200, 90, 180);
        g.drawArc(725, 100, 150, 200, 270, 180);
    }

    /**
     * Paints a small plane
     * @param g the graphics on which to draw
     */
    public void paintSmallPlane(Graphics g)
    {
        g.drawLine(200, 125, 700, 125);
        g.drawLine(200, 275, 700, 275);
        g.drawArc(0, 125, 400, 150, 90, 180);
        g.drawLine(350, 125, 350, 275);
        g.drawLine(200, 125, 200, 275);
        g.drawLine(315, 125, 315, 275);
        g.drawLine(698, 125, 698, 275);
        g.drawArc(625, 125, 150, 150, 270, 180);
    }

    public void paintLegend(Graphics g)
    {
        g.drawString("Free seat:", 5, 33);
        g.drawRect(90, 15, 18, 18);
        g.drawString("Taken seat:", 5, 63);
        g.drawRect(90, 45, 18, 18);
        g.drawLine(90, 45, 108, 63);
        g.drawLine(90, 63, 108, 45);
        g.drawString("Chosen seat:", 5, 93);
        g.drawRect(90, 81, 18, 18);
        g.fillRect(90, 81, 18, 18);
        
        g.drawString("First class:", 150, 33);
        g.drawRect(260, 15, 25, 25);
        g.drawString("Business class:", 150, 63);
        g.drawRect(260, 45, 22, 22);
        g.drawString("Economy class:", 150, 93);
        g.drawRect(260, 75, 18, 18);
    }
    /**
     * Firstly, draws the seat from the coordinates retrieved from the
     * controller. Secondly, draws a mark on those seats, which are already
     * taken. Thirdly, if the seats is currently selected, it is filled
     * @param g the graphics on which to draw
     */
    public void drawSeats(Graphics g)
    {
        for (Seat current : controller.getDeparture().getSeats())
        {

            g.drawRect(current.getX(), current.getY(),
                    current.getSeatType().getWidth(),
                    current.getSeatType().getHeight());
            if (current.isOccupied())
            {
                g.drawLine(current.getX(), current.getY(),
                        current.getX() + current.getSeatType().getWidth(),
                        current.getY() + current.getSeatType().getHeight());
                g.drawLine(current.getX() + current.getSeatType().getWidth(),
                        current.getY(), current.getX(),
                        current.getY() + current.getSeatType().getHeight());
            } else
            {
                if (controller.getChosenSeats().contains(current))
                {
                    g.fillRect(current.getX(), current.getY(),
                            current.getSeatType().getWidth(),
                            current.getSeatType().getHeight());
                }
            }
        }
    }
}