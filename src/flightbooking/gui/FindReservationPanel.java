package flightbooking.gui;

import flightbooking.Controller;
import flightbooking.Departure;
import flightbooking.Reservation;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

public class FindReservationPanel extends JPanel implements GUIPanel
{

    private Controller controller;
    private DepartureTable departureTable;
    private ArrayList<Reservation> reservations;

    public FindReservationPanel(Controller controller)
    {
        this.controller = controller;
        departureTable = new DepartureTable();
    }

    @Override
    public JPanel getPanel()
    {
        return this;
    }

    @Override
    public void setup()
    {
        removeAll();
        setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
        JPanel input = new JPanel();
        input.setLayout(new FlowLayout());
        JLabel label = new JLabel("Enter phone number here:");
        input.add(label);
        final JTextField number = new JTextField(20);

        input.add(number);
        JButton search = new JButton("Search");
        search.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                try
                {
                    String phone = number.getText();
                    if (Validation.isPhoneNumberValid(phone) == false)
                    {
                        throw new NullPointerException("Please enter a"
                                + " valid number. Only numbers are allowed");
                    }
                    reservations = controller.getDeparturesByPhone(phone);
                    if (reservations == null || reservations.isEmpty())
                    {
                        throw new NullPointerException("No reservations found.");
                    }
                }
                catch (NullPointerException excep)
                {
                    reservations = new ArrayList<>();
                    Validation.validationError(excep.getMessage());
                }
                finally
                {
                    update();
                }
            }
        });
        input.add(search);
        add(input);
        JScrollPane scroller = new JScrollPane(departureTable);
        add(scroller);
    }

    @Override
    public void update()
    {
        ArrayList<Departure> temp = new ArrayList<>();
        for (Reservation res : reservations)
        {
            temp.add(res.getDeparture());
        }
        departureTable.fill(temp);
        revalidate();
    }

    @Override
    public void validate()
    {
        try
        {
            controller.setReservation(reservations.get(
                    departureTable.getSelected()));
        }
        catch (NullPointerException excep)
        {
            Validation.validationError(excep.getMessage());
        }
    }
}
