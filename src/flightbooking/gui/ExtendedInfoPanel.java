package flightbooking.gui;

import flightbooking.Controller;
import flightbooking.Customer;

public class ExtendedInfoPanel extends InfoPanel
{

    public ExtendedInfoPanel(Controller controller)
    {
        super(controller);

        addLabelField("First name", "firstName", true);
        addLabelField("Last name", "lastName", true);
        addLabelField("Adress", "adress", true);
        addLabelField("Phone number", "phone", true);
        addLabelField("Email adress", "email", true);
    }

    @Override
    public void setup()
    {
        super.setup();

        Customer customer = null;
        if (controller.getReservation() != null)
        {
            customer = controller.getReservation().getCustomer();
        }
        else
        {
            if (controller.getCustomer() != null)
            {
                customer = controller.getCustomer();
            }
        }

        if (customer != null)
        {
            fields.get("firstName").setText(customer.getFirstName());
            fields.get("lastName").setText(customer.getLastName());
            fields.get("adress").setText(customer.getAdress());
            fields.get("phone").setText(customer.getPhone());
            fields.get("email").setText(customer.getMail());
        }
    }

    public void sendCustomer()
    {
        try
        {
            boolean noError = true;
            String errorMessage = "Please enter a valid";
            String firstName = fields.get("firstName").getText();
            if (Validation.isNameValid(firstName) == false)
            {
                errorMessage += " first name";
                noError = false;
            }

            String lastName = fields.get("lastName").getText();
            if (Validation.isNameValid(lastName) == false)
            {
                if (noError == false)
                {
                    errorMessage += ",";
                }
                errorMessage += " last name";
                noError = false;
            }

            String adress = fields.get("adress").getText();

            String phone = fields.get("phone").getText();
            if (Validation.isPhoneNumberValid(phone) == false)
            {
                if (noError == false)
                {
                    errorMessage += ",";
                }
                errorMessage += " phone number";
                noError = false;
            }

            String email = fields.get("email").getText();
            if (Validation.isEMailValid(email) == false)
            {
                if (noError == false)
                {
                    errorMessage += ",";
                }
                errorMessage += " email";
                noError = false;
            }
            if (noError == false)
            {
                throw new IllegalArgumentException(errorMessage);
            }
            else
            {
                controller.setCustomer(firstName, lastName, adress,
                        phone, email);
                controller.getGUIController().next();
            }
        }
        catch (IllegalArgumentException excep)
        {
            Validation.validationError(excep.getMessage());
        }
    }
}
