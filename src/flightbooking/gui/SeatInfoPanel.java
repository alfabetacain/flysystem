package flightbooking.gui;

import flightbooking.Controller;
import flightbooking.Seat;
import java.util.Iterator;

/**
 * The SeatInfoPanel displays information about which seats are selected
 * Furthermore it wraps a InfoPanel which displays basic information about the
 * chosen departure
 */
public class SeatInfoPanel extends InfoPanel {

    /**
     * Constructs the SeatInfoPanel. In the constructor it furthermore creates a
     * InfoPanel and adds the InfoPanel to itself
     *
     * @param controller the controller, which contains the data to display
     */
    public SeatInfoPanel(Controller controller) {
        super(controller);
        addLabelField("Chosen seats", "chosen", false);
    }

    /**
     * Sets up the display and gathers the information necessary to do so
     */
    @Override
    public void setup() {
        super.setup();
        setChosen();
    }

    /**
     * Updates its display to reflect new or changed data
     */
    public void update() {
        setChosen();
    }
    
    private void setChosen() {
        
        String chosen = "";
        
        Iterator<Seat> iterator = controller.getChosenSeats().iterator();
        while (iterator.hasNext()) {
            Seat seat = iterator.next();
            chosen += seat.getRow() + seat.getLetter();
            if (iterator.hasNext()) {
                chosen += ", ";
            }
        }
        
        fields.get("chosen").setText(chosen);
    }
}