package flightbooking.gui;

import javax.swing.JPanel;

/**
 *Interface for the windows used in the gui
 */
public interface DisplayWindow
{
    public void setContent(JPanel panel);
}