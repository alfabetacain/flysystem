package flightbooking.gui;

import flightbooking.Departure;
import java.awt.Dimension;
import java.util.ArrayList;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class DepartureTable extends JTable
{

    private String[] columns;

    public DepartureTable()
    {

        super();

        // Columns
        this.columns = new String[]
        {
            "ID",
            "From",
            "To",
            "Time of Departure",
            "Time of Arrival",
            "Available Seats"
        };

        // Set the model with no data
        this.setModel(new DefaultTableModel(null, columns));

        // Setting preferences for the table
        this.setShowGrid(false);
        this.setPreferredScrollableViewportSize(new Dimension(1024, 600));
        this.setFillsViewportHeight(true);
        this.setAutoCreateRowSorter(true);
    }

    /**
     * Reformat the list of departures to a two-dimensional string array for
     * display in the table.
     *
     * @param departures a list of departures
     */
    public void fill(ArrayList<Departure> departures)
    {

        // Make and fill array
        String[][] data = new String[departures.size()][columns.length];
        for (int i = 0; i < departures.size(); i++)
        {

            Departure departure = departures.get(i);

            // Departure id
            data[i][0] = Integer.toString(departure.getId());

            // Airports
            data[i][1] = departure.getOrigin().toString();
            data[i][2] = departure.getDestination().toString();

            // Departure and arrival datetimes
            data[i][3] = departure.getDeparture().toString();
            data[i][4] = departure.getArrival().toString();

            // Seats available
            data[i][5] = Integer.toString(departure.seatsAvailable());
        }

        // Set data
        this.setModel(new DefaultTableModel(data, columns));
    }

    public int getSelected() throws NullPointerException
    {
        int selected = this.getSelectedRow();
        if (selected == -1)
        {
            throw new NullPointerException("Nothing selected");
        }
        return this.convertRowIndexToModel(selected);
    }

    @Override
    public boolean isCellEditable(int row, int col)
    {
        return false;
    }
}