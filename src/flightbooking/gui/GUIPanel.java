package flightbooking.gui;

import javax.swing.JPanel;

/**
 *
 * Interface for panels, which will be shown.
 */
public interface GUIPanel
{
    //Returns the panel, so it can be placed in a frame
    public JPanel getPanel();
    //Sets up the contents of the panel
    public void setup();
    //Updates the contents
    public void update();
    
    public void validate();
}
