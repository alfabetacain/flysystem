package flightbooking.gui;

import flightbooking.Controller;
import flightbooking.Passenger;
import flightbooking.Seat;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.util.ArrayList;
import java.util.Iterator;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

public class PassengerFormPanel extends JPanel implements GUIPanel
{

    private Controller controller;
    private ArrayList<Seat> seats;
    private ArrayList<JTextField> firstNames, lastNames, seatNumbers;

    public PassengerFormPanel(Controller controller)
    {
        this.controller = controller;
        this.firstNames = new ArrayList<>();
        this.lastNames = new ArrayList<>();
        this.seatNumbers = new ArrayList<>();
    }

    @Override
    public JPanel getPanel()
    {
        return this;
    }

    @Override
    public void setup()
    {

        // Wipe
        this.removeAll();
        firstNames.clear();
        lastNames.clear();
        seatNumbers.clear();

        JPanel panel = new JPanel();
        panel.setLayout(new GridLayout(0,3,5,5));
        
        JScrollPane scroller = new JScrollPane(panel, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, 
                                                JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);        
        // Add labels
        panel.add(new JLabel("First name"));
        panel.add(new JLabel("Last name"));
        panel.add(new JLabel("Seat"));
        
        // Get seats
        seats = controller.getChosenSeats();
        Iterator<Seat> seatIterator = seats.iterator();

        // Get passengers
        ArrayList<Passenger> passengers = controller.getPassengers();
        Iterator<Passenger> passengerIterator = null;
        if (passengers != null)
        {
            passengerIterator = passengers.iterator();
        }

        // Add fields
        while (seatIterator.hasNext())
        {

            JTextField firstName = new JTextField();
            panel.add(firstName);
            firstNames.add(firstName);
            
            JTextField lastName = new JTextField();
            lastNames.add(lastName);
            panel.add(lastName);
            
            // Fill in passenger info if available
            if (passengerIterator != null && passengerIterator.hasNext())
            {
                Passenger passenger = passengerIterator.next();
                firstName.setText(passenger.getFirstName());
                lastName.setText(passenger.getLastName());
            }

            Seat seat = seatIterator.next();

            JTextField seatNumber = new JTextField(seat.getRow() + seat.getLetter());
            seatNumber.setEnabled(false);
            seatNumbers.add(lastName);
            panel.add(seatNumber);
        }
        if(panel.getPreferredSize().height > 500)
        {
            scroller.setPreferredSize(new Dimension(300 ,500));
        } 
        else  
        {
            scroller.setPreferredSize(new Dimension(300, 
                                            panel.getPreferredSize().height+5));
        }
        scroller.setBorder(null);
        this.add(scroller);
    }

    @Override
    public void validate()
    {
        try
        {
            
           for(int i = 0; i < firstNames.size(); i++)
           {
                if (Validation.isNameValid(firstNames.get(i).getText()) == false)
                {
                    throw new IllegalArgumentException("Please enter a valid "
                            + "first name in the form for passenger "
                            + "number " + (firstNames.indexOf(firstNames.get(i)) + 1));
                }
                        if (Validation.isNameValid(lastNames.get(i).getText()) == false)
                {
                    throw new IllegalArgumentException("Please enter a valid "
                            + "last name in the form for passenger "
                            + "number " + (lastNames.indexOf(lastNames.get(i)) + 1));
                }
           }
            String[][] names = new String[2][seats.size()];
            for (int i = 0; i < seats.size(); i++)
            {
                names[0][i] = firstNames.get(i).getText();
                names[1][i] = lastNames.get(i).getText();
            }
            controller.setPassengers(names[0], names[1]);
        }
        catch (IllegalArgumentException excep)
        {
            Validation.validationError(excep.getMessage());
        }
    }

    @Override
    public void update()
    {
    }
}