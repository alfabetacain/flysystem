package flightbooking.gui;

import flightbooking.Controller;
import flightbooking.Departure;
import java.util.ArrayList;
import java.util.HashMap;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SpringLayout;

/**
 * InfoPanel displays information gathered from the current knowledge of a given
 * reservation. It contains a minimum of information and is usually wrapped in
 * other classes, to increase the amount of information
 */
public class InfoPanel extends JPanel {
    
    protected Controller controller;
    protected ArrayList<JLabel> labels;
    protected HashMap<String, JTextField> fields;
    protected int rows;
    
    /**
     * Constructs the InfoPanel. It creates a number of JTextFields in a grid
     * layout
     */
    public InfoPanel(Controller controller) {
        this.controller = controller;
        
        this.setLayout(new SpringLayout());
        
        labels = new ArrayList<>();
        fields = new HashMap<>();
        
        addLabelField("Origin airport", "origin", false);
        addLabelField("Destination airport", "destination", false);
        addLabelField("Time of departure", "departure", false);
        addLabelField("Time of arrival", "arrival", false);
        addLabelField("Airline company", "company", false);
    }

    public void addLabelField(String labelText, String fieldKey, boolean editable) {

        JTextField field = new JTextField();
        if (!editable) {
            field.setEditable(false);
        }
        fields.put(fieldKey, field);
        
        JLabel label = new JLabel(labelText);
        label.setLabelFor(field);
        labels.add(label);
        
        this.add(label);
        this.add(field);
        
        rows++;
    }

    /**
     * Sets up the InfoPanel. Because the InfoPanel is created from the
     * beginning, it is not certain that all information are present when
     * created. Therefore a setup method exists, so that when setup is called it
     * is because the information is now ready
     *
     * @param controller the controller, from which the information comes
     */
    public void setup() {
        Departure departure;
        if (controller.getReservation() == null) {
            departure = controller.getDeparture();
        } else {
            departure = controller.getReservation().getDeparture();
        }
        
        fields.get("origin").setText(departure.getOrigin().toString());
        fields.get("destination").setText(departure.getDestination().toString());
        fields.get("departure").setText(departure.getDeparture().toString());
        fields.get("arrival").setText(departure.getArrival().toString());
        fields.get("company").setText("Olga Airlines");
        
        SpringUtilities.makeCompactGrid(this, rows, 2, 0, 0, 5, 5);
    }
}
