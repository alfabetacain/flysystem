package flightbooking.gui;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JPanel;

/**
 * The primary panel, which is the first to be displayed From here you choose
 * what you want to do. This panel will always remain open and must not be
 * closed. The button are arranged in a 2x2 pattern (possibly an exit option
 * should be implemented)
 */
public class InitialPanel extends JPanel
{

    /**
     * Constructs the panel
     *
     * @param controller the controller which controls the flow of the gui
     */
    public InitialPanel()
    {
        
    }

    public void setup(final GUIController gui)
    {
        JPanel buttonsPanel = new JPanel();
        GridLayout layout = new GridLayout(0, 2);
        buttonsPanel.setLayout(layout);

        JButton makeRes = new JButton("Make reservation");
        makeRes.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                gui.makeRes();
            }
        });
        buttonsPanel.add(makeRes);

        JButton findRes = new JButton("Find reservation");
        findRes.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                gui.findRes();
            }
        });
        buttonsPanel.add(findRes);

        add(buttonsPanel, BorderLayout.CENTER);
    }
}