package flightbooking.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;

/**
 * This class validates the users input in the different textfield in the
 * program.
 */
public class Validation{
    
    public Validation()
    {
    }
    
    /**
     * This is an error window which appears if the validation fails. 
     * 
     * @param errorMessage - Message for the JLabel in the window.
     * @return JFrame
     */
    public static JFrame validationError(String errorMessage)
    {
        final JFrame frame = new JFrame();
        frame.setTitle("Error");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLocationRelativeTo(null);
       
        
        Border padding = BorderFactory.createEmptyBorder(10, 10, 10, 10);
        
        JLabel label = new JLabel(errorMessage, JLabel.CENTER);
        JPanel labelPanel = new JPanel();
        labelPanel.add(label);
        labelPanel.setBorder(padding);
        
        JButton okButton = new JButton("OK");
        okButton.setActionCommand("ok");
        okButton.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                frame.dispose();
            }
        });        
        
        JPanel buttonPane = new JPanel();
        buttonPane.add(okButton);
        
        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.PAGE_AXIS));
        panel.add(labelPanel);
        panel.add(buttonPane);
        frame.add(panel);
        frame.pack();
        frame.setVisible(true);
        return frame;
    }
    
    /**
     * This method validates a name.
     * 
     * @param s - The String which is being validated.
     * @return boolean - true if validated, false otherwise
     */
    public static boolean isNameValid(String s)
    {
        String regEx = "[A-Za-z]+";
        return s.matches(regEx);
    }
    
    /**
     * This method validates a phone number.
     * 
     * @param s - The String which is being validated.
     * @return boolean - true if validated, otherwise false
     */
    public static boolean isPhoneNumberValid(String s)
    {
        String regEx = "[\\+]?" + "[0-9]+";
        return s.matches(regEx);
    }
    
    /**
     * This method validates an e-mail adress.
     * 
     * @param s - The String which is being validated.
     * @return boolean - true if validated, otherwise false
     */
    public static boolean isEMailValid(String s)
    {
        String regEx = "[_\\w\\d\\-]+(\\.[_\\w\\d]+)*@" + 
                        "[\\w\\d\\-]+(\\.[\\w\\d]+)*(\\.[A-Za-z]{2,})";
        return s.matches(regEx);
    }
    
    /**
     * This method validates if a date has the following format: yyyy-MM-dd
     * @param s - The String which is being validated.
     * @return boolean - true if validated, otherwise false
     */
    public static boolean isDateValid(String s)
    {
        String regEx = "[_0-9]{4}"+"[\\-]"+"[0-9]{2}"+"[\\-]"+"[0-9]{2}";
        return s.matches(regEx);
    }
}
