package flightbooking.gui;

import flightbooking.Controller;
import java.awt.BorderLayout;
import javax.swing.JPanel;

public class CustomerPanel extends JPanel implements GUIPanel
{
    private ExtendedInfoPanel customerForm;
    
    public CustomerPanel(final Controller controller)
    {
        setLayout(new BorderLayout());
        customerForm = new ExtendedInfoPanel(controller);    
    }
    @Override
    public JPanel getPanel()
    {
        return this;
    }

    @Override
    public void setup()
    {
        customerForm.setup();
        add(customerForm);
    }

    @Override
    public void update()
    {
        
    }
    
    @Override
    public void validate()
    {
        //Do some validation here
        
        customerForm.sendCustomer();
    }

}