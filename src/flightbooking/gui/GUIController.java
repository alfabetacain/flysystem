package flightbooking.gui;

import flightbooking.Controller;
import java.awt.BorderLayout;
import java.util.ArrayList;
import javax.swing.JPanel;

/**
 * The purpose of GUIController is to contain the the temporary information
 * which will eventually become the final reservation, and to control the flow
 * of the GUI windows. It is also an actionlistener so it is informed when the
 * button to be, which will contain the text "next", is pressed
 */
public class GUIController
{

    private final Controller controller;
    private InitialPanel initialPanel;
    private final NavigationPanel nav;
    private ArrayList<GUIPanel> steps;
    private int counter;
    private final DisplayWindow window;

    /**
     * Constructor, which sets the default work space
     *
     * @param seats
     * @param type
     * @param dep
     */
    //public GUIController(ArrayList<Seat> seats, PlaneType type, Departure dep)
    public GUIController(Controller controller)
    {
        this.controller = controller;
        initialPanel = new InitialPanel();
        nav = new NavigationPanel();
        window = new Window();
    }

    public void setup()
    {
        initialPanel.setup(this);
        nav.setup(controller);
        window.setContent(initialPanel);
    }

    /**
     * Sets up the sequence for making a reservation
     */
    public void makeRes()
    {
        //Fill arraylist
        steps = new ArrayList<>();
        steps.add(new DeparturePanel(controller));
        steps.add(new SeatChoosePanel(controller));
        steps.add(new PassengerFormPanel(controller));
        steps.add(new CustomerPanel(controller));
        steps.add(new FinalReviewPanel(controller));
        //sets counter to zero which corresponds to first in the arraylist
        counter = 0;
        setStep();
    }

    /**
     * Sets up the sequence for finding and deleting a reservation
     */
    public void findRes()
    {
        steps = new ArrayList<>();
        steps.add(new FindReservationPanel(controller));
        steps.add(new FindReservationFinalReviewPanel(controller));
        counter = 0;
        setStep();
    }

    public void next()
    {
        try
        {
            ++counter;
            if (counter < 0 || counter >= steps.size())
            {
                throw new ArrayIndexOutOfBoundsException("Out of bounds in steps");
            }
            setStep();
        }
        catch (ArrayIndexOutOfBoundsException excep)
        {
            System.out.println(excep.toString());
            counter = 0;
            window.setContent(initialPanel);

        }
    }

    public void previous()
    {
        if (counter >= 0)
        {
            --counter;
            setStep();
        }
    }

    public void setStep()
    {
        GUIPanel guiPanel = steps.get(counter);
        guiPanel.setup();
        JPanel panelToSend = new JPanel();
        panelToSend.setLayout(new BorderLayout());
        panelToSend.add(guiPanel.getPanel(), BorderLayout.CENTER);
        if (counter >= 0)
        {
            if (counter >= 1)
            {
                nav.enablePrevious();
            }
            else
            {
                nav.disablePrevious();
            }
            panelToSend.add(nav, BorderLayout.SOUTH);
        }
        window.setContent(panelToSend);
    }

    public void updateStep()
    {
        steps.get(counter).update();
    }

    public void endSession()
    {
        window.setContent(initialPanel);
        nav.changeToNext();
        counter = 0;
    }
    
    public void makeReadyForDelete()
    {
        nav.changeToDelete();
        next();
    }

    public GUIPanel getCurrentPanel()
    {
        return steps.get(counter);
    }
}