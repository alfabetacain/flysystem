package flightbooking.gui;

import flightbooking.Controller;
import java.awt.BorderLayout;
import javax.swing.JPanel;

/**
 * The SeatChoosePanel class is responsible for setting up the necessary
 * components for making the gui panel ready
 */
public class SeatChoosePanel extends JPanel implements GUIPanel
{

    private SeatInfoPanel upperInfoWindow;
    private PlaneDrawingPanel planePlan;
    private GUIController gui;

    /**
     * Constructs the SeatChoosePanel. It gathers all components it needs
     * without giving any specific data on how they should look
     * @param controller the controller to notify on changes
     */
    public SeatChoosePanel(final Controller controller)
    {
        gui = controller.getGUIController();
        setLayout(new BorderLayout());
        upperInfoWindow = new SeatInfoPanel(controller);
        add(upperInfoWindow, BorderLayout.NORTH);
        planePlan = new PlaneDrawingPanel(controller);
        add(planePlan, BorderLayout.CENTER);
        //chosenSeatsWindow = new ChosenSeatsWindow();
    }
    
    /**
     * Get the JPanel object
     * @return itself, a JPanel object
     */
    @Override
    public JPanel getPanel()
    {
        return this;
    }

    /**
     * Informs its components and itself that the needed data for the complete
     * setup is now available
     */
    @Override
    public void setup()
    {
        upperInfoWindow.setup();
    }

    /**
     * Updates its components to reflect changes in the data
     */
    @Override
    public void update()
    {
        planePlan.repaint();
        upperInfoWindow.update();
    }
    
    @Override
    public void validate()
    {
        gui.next();
    }
}