package flightbooking.gui;

import flightbooking.Airport;
import flightbooking.Controller;
import flightbooking.Departure;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

import java.util.Calendar;
import java.util.Date;
import javax.swing.BoxLayout;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

public class DeparturePanel extends JPanel implements GUIPanel
{

    private Controller controller;
    private String[] airportStrings;
    private ArrayList<Airport> airports;
    private DepartureTable table;
    private JScrollPane tableScroller;
    private ArrayList<Departure> departures;

    /**
     * This is the constructor for the Tables class. It's purpose is to take the
     * information given from the controller and make that into a table.
     *
     * @param airports a list of airports
     */
    public DeparturePanel(Controller controller)
    {
        this.controller = controller;
        createTablePane();
        this.setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
    }

    private void createTablePane()
    {

        table = new DepartureTable();

        // Wrap the table in a scroller pane
        tableScroller = new JScrollPane(table);
    }

    /**
     * Sends the selected departure to the controller.
     */
    public void sendDeparture()
    {
        int index = table.getSelected();
        controller.setDeparture(departures.get(index));
    }

    /**
     * Makes a panel containing input components for departure search.
     *
     * @return
     */
    public JPanel inputPanel()
    {

        ArrayList<Component> components = new ArrayList<>();

        // Origin combobox
        components.add(new JLabel("Origin"));
        final JComboBox cbOrigin = new JComboBox(airportStrings);
        components.add(cbOrigin);

        // Destination combobox
        components.add(new JLabel("Destination"));
        final JComboBox cbDestination = new JComboBox(airportStrings);
        components.add(cbDestination);

        // Date textfield
        components.add(new JLabel("Date (year-month-date)"));
        final JTextField tfDate = new JTextField(10);
        components.add(tfDate);

        // Search button
        JButton bSearch = new JButton("Search");
        components.add(bSearch);

        // Add an action listener to the search button
        bSearch.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {

                try
                {
                    // Get string from date textfield
                    String sDate = tfDate.getText();
                    if (Validation.isDateValid(sDate) == false)
                    {
                        throw new IllegalArgumentException("Please enter a "
                                + "valid date in the format yyyy-mm-dd");
                    }
                    // Convert string to date
                    SimpleDateFormat simpDate = new SimpleDateFormat("yyyy-MM-dd");
                    Date departureDate = null;
                    try
                    {
                        departureDate = simpDate.parse(sDate);
                    }
                    catch (java.text.ParseException ex)
                    {
                        System.out.println("Parse exception: " + ex);
                    }
                    // Get index of selected airports
                    int origin = cbOrigin.getSelectedIndex();
                    int destination = cbDestination.getSelectedIndex();


                    // Make an arrival date one day later
                    Calendar calendar = Calendar.getInstance();
                    calendar.setTime(departureDate);
                    calendar.add(Calendar.DATE, 1);
                    Date arrivalDate = calendar.getTime();

                    // Get departures from server
                    departures = controller.getDepartures(
                            departureDate,
                            arrivalDate,
                            airports.get(origin),
                            airports.get(destination));

                }
                catch (IllegalArgumentException excep)
                {
                    departures = new ArrayList<>();
                    Validation.validationError(excep.getMessage());
                }
                finally
                {
                    //Perform an update to the panel
                    update();
                }
            }
        });

        // Create a new panel for the input components
        JPanel inputs = new JPanel();

        // Add the components to the panel
        for (Component component : components)
        {
            inputs.add(component);
        }

        // Return panel
        return inputs;
    }

    public void airportsToStrings()
    {
        airportStrings = new String[airports.size()];
        for (int i = 0; i < airports.size(); i++)
        {
            airportStrings[i] = airports.get(i).toString();
        }
    }

    @Override
    public JPanel getPanel()
    {
        return this;
    }

    @Override
    public void setup()
    {
        this.airports = controller.getAirports();
        airportsToStrings();
        removeAll();
        add(inputPanel());
        add(tableScroller);
    }

    @Override
    public void update()
    {
        table.fill(departures);
        revalidate();
    }

    @Override
    public void validate()
    {
        try
        {
            sendDeparture();
        }
        catch (NullPointerException excep)
        {
            Validation.validationError(excep.getMessage());
        }

    }
}