package flightbooking.gui;

import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 * The window class, which will work as frames
 */
public class Window extends JFrame implements DisplayWindow
{
    /**
     * Constructs the window with basic information about size etc.
     */    
    public Window()
    {
        setTitle("Flight Booking");
        setResizable(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
    
    /**
     * Sets the contentPane of this JFrame
     * @param panel the panel the contentPane will contain
     */
    @Override
        public void setContent(JPanel panel)
    {
        setContentPane(panel);
        pack();
        setVisible(true);
    }
}