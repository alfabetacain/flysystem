package flightbooking;

import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

public class SQLController {

    SQLGenerator generator;
    SQLExecutor executor;
    SQLMapper mapper;

    public SQLController() {
        this.generator = new SQLGenerator();
        this.executor = SQLExecutor.get();
        this.mapper = new SQLMapper();
    }

    /**
     * Resets all tables by dropping them and creating them again.
     */
    public void reset() {
        executor.executeBatch(generator.getSQLList("dropTables"));
        executor.executeBatch(generator.getSQLList("createTables"));
        executor.executeBatch(generator.getSQLList("fillTables"));
        saveSeats(SeatCreator.getSeats());
    }

    /**
     * Gets all departures that fit within the given criteria.
     *
     * @param departure
     * @param arrival
     * @param origin
     * @param destination
     * @return a list of departures
     */
    public ArrayList<Departure> getDepartures(Date departure, Date arrival, Airport origin, Airport destination) {
        String sql = generator.getDepartures(departure, arrival, origin, destination);
        Statement statement = executor.executeQuery(sql);
        ArrayList<Departure> departures = mapper.getDepartures(statement);

        // Get seats for each departure
        for (Departure dep : departures) {
            dep.setSeats(getSeats(dep));
        }

        return departures;
    }

    /**
     * Gets all seats on the given departure.
     *
     * @param departure
     * @return a list of seats
     */
    public ArrayList<Seat> getSeats(Departure departure) {
        String sql = generator.getSeats(departure.getPlane().getType());
        Statement statement = executor.executeQuery(sql);
        ArrayList<Seat> seats = mapper.getSeats(statement);

        // Get passengers for departure
        ArrayList<Passenger> passengers = getPassengers(departure);

        // Mark seats with passengers as occupied
        for (Seat seat : seats) {
            // System.out.print(seat.getId() + ": " + seat.isOccupied() + " -> ");
            for (Passenger passenger : passengers) {
                if (seat.getId() == passenger.getSeat().getId()) {
                    seat.setOccupied(true);
                }
            }
            // System.out.println(seat.isOccupied());
        }

        return seats;
    }

    /**
     * Gets all Passengers associated with a Departure.
     *
     * @param departure a Departure
     * @return a list of passengers
     */
    public ArrayList<Passenger> getPassengers(Departure departure) {
        String sql = generator.getPassengers(departure);
        Statement statement = executor.executeQuery(sql);
        return mapper.getPassengers(statement);
    }
    
    /**
     * Gets all Passengers associated with a Reservation.
     *
     * @param departure a Reservation
     * @return a list of passengers
     */
    public ArrayList<Passenger> getPassengers(Reservation reservation) {
        String sql = generator.getPassengers(reservation);
        Statement statement = executor.executeQuery(sql);
        return mapper.getPassengers(statement);
    }

    /**
     * Gets all airports.
     *
     * @return a list of airports
     */
    public ArrayList<Airport> getAirports() {
        String sql = generator.getSQLSingle("getAirports");
        Statement statement = executor.executeQuery(sql);
        return mapper.getAirports(statement);
    }
    
    /**
     * Gets a customer with the given phone number.
     * 
     * @param phone the phone number associated with the customer
     * @return the customer, or null if no customer was found
     */
    public Customer getCustomer(String phone) {
        String sql = generator.getCustomer(phone);
        Statement statement = executor.executeQuery(sql);
        return mapper.getCustomer(statement);
    }

    public ArrayList<Reservation> getReservations(Customer customer) {
        String sql = generator.getReservations(customer.getId());
        Statement statement = executor.executeQuery(sql);
        ArrayList<Reservation> reservations = mapper.getReservations(statement);
        for (Reservation reservation : reservations) {
            reservation.setCustomer(customer);
            reservation.setPassengers(getPassengers(reservation));
            reservation.getDeparture().setSeats(getSeats(reservation.getDeparture()));
        }
        return reservations;
    }
    
    /**
     * Saves reservation to database.
     * 
     * @param reservation 
     */
    public void saveReservation(Reservation reservation) {
        String sql;
        // Save customer and get id
        int customerId;
        Customer customer = getCustomer(reservation.getCustomer().getPhone());
        if (customer != null) {
            customerId = customer.getId();
        } else {
            sql = generator.saveCustomer(reservation.getCustomer());
            customerId = executor.executeUpdate(sql);
        }
        // Save reservation and get id
        int departureId = reservation.getDeparture().getId();
        sql = generator.saveReservation(departureId, customerId);
        int reservationId = executor.executeUpdate(sql);
        reservation.setId(reservationId);
        // Save passengers
        savePassengers(reservation.getPassengers(), reservation);
    }
    
    public void savePassengers(ArrayList<Passenger> passengers, Reservation reservation) {
        String sql = generator.savePassengers(passengers, reservation.getId());
        executor.executeUpdate(sql);
    }
    
    /**
     * Saves seats to database.
     * 
     * @param seats seats to be saved
     */
    public void saveSeats(ArrayList<Seat> seats) {
        String sql = generator.saveSeats(seats);
        executor.executeUpdate(sql);
    }
    
    /**
     * Deletes a Reservation from the database.
     * 
     * @param reservation a reservation
     */
    public void deleteReservation(Reservation reservation) {
        String sql = generator.deleteReservation(reservation);
        executor.executeUpdate(sql);
    }
    
    /**
     * Deletes a Passenger from the database.
     * 
     * @param passenger a Passenger
     */
    public void deletePassenger(Passenger passenger) {
        String sql = generator.deletePassenger(passenger);
        executor.executeUpdate(sql);
    }
    
    public void updatePassenger(Passenger passenger) {
        String sql = generator.updatePassenger(passenger);
        executor.executeUpdate(sql);
    }
}