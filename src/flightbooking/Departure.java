package flightbooking;

import java.util.ArrayList;
import java.util.Date;

/**
 * The class departure is equal to the departure entity in 
 * the database
 */
public class Departure {

    private int id;
    private Plane plane;
    private Date departure, arrival;
    private Airport origin, destination;
    private ArrayList<Seat> seats;

    /**
     * Constructor. Creates an departure object from the given 
     * parameters
     * @param id the id of the departure
     * @param plane the plane of the departure
     * @param departure the date of the departure
     * @param arrival the arrival date of the departure
     * @param origin the airport from which to depart
     * @param destination the destination to arrive to
     */
    public Departure(int id, Plane plane, Date departure, Date arrival, Airport origin, Airport destination) {
        this.id = id;
        this.plane = plane;
        this.departure = departure;
        this.arrival = arrival;
        this.origin = origin;
        this.destination = destination;
    }

    /**
     * Returns the size of the list in the seats field
     * @return size of seats list
     */
    public int seatsTotal() {
        return seats.size();
    }
    
    public int seatsAvailable() {
        int available = seatsTotal();
        for (Seat seat : seats) {
            if (seat.isOccupied()) {
                available--;
            } 
        }
        return available;
    }
    
    public void setId(int id) {
        this.id = id;
    }

    public void setPlane(Plane plane) {
        this.plane = plane;
    }

    public void setDeparture(Date departure) {
        this.departure = departure;
    }

    public void setArrival(Date arrival) {
        this.arrival = arrival;
    }

    public void setOrigin(Airport origin) {
        this.origin = origin;
    }

    public void setDestination(Airport destination) {
        this.destination = destination;
    }

    public void setSeats(ArrayList<Seat> seats) {
        this.seats = seats;
    }
    
    public int getId() {
        return id;
    }

    public Plane getPlane() {
        return plane;
    }

    public Date getDeparture() {
        return departure;
    }

    public Date getArrival() {
        return arrival;
    }

    public Airport getOrigin() {
        return origin;
    }

    public Airport getDestination() {
        return destination;
    }
    
    public ArrayList<Seat> getSeats() {
        return seats;
    }
    
}
