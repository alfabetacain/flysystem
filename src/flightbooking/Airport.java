package flightbooking;

/**
 * This class is meant to the result of mapping the airport
 * entity to the java environment. It contains the same information.
 */
public class Airport {

    private int id;
    private String name, country, city;

    /**
     * Constructor, which sets the fields.
     * @param id an id, which is used in the database as a primary key
     * @param name the name of the airport
     * @param country the country in which the airport is located
     * @param city  the city in which the airport is located
     */
    public Airport(int id, String name, String country, String city) {
        this.id = id;
        this.name = name;
        this.country = country;
        this.city = city;
    }

    /**
     * Converts the basic information about the airport to a single
     * string
     * @return a compilation of information about the airport 
     */
    @Override
    public String toString() {
        String format = "%s, %s, %s";
        return String.format(
                format,
                this.getName(),
                this.getCity(),
                this.getCountry());
    }

    /**
     * Returns the content of the name field
     * @return the name field content
     */
    public String getName() {
        return name;
    }
    
    /**
     * Returns the content of the country field
     * @return the country field content
     */
    public String getCountry() {
        return country;
    }

    /**
     * Returns the content of the city field
     * @return the city field content
     */
    public String getCity() {
        return city;
    }
}